# create a fastapi server with an endpoint to return 'say hi'
import uvicorn
from fastapi import FastAPI

# import BaseModel from pydantic
from pydantic import BaseModel

# create a book class with two fields name and description
class Book(BaseModel):
    name: str
    description: str

# create a get endpoint and return a random book
app = FastAPI()

@app.get("/")
def read_root():
    return {"message": "Hello World"}

@app.get("/book")
def get_book():
    # return a book of Leo Tolstoy
    return Book(name="War and Peace", description="Leo Tolstoy's epic novel")

# run the server
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)

